<?php

use yii\db\Schema;
use yii\db\Migration;

class m150310_112004_create_news_table extends Migration
{
    public function up()
    {

        $this->createTable('news', [

            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'preview' => Schema::TYPE_TEXT,
            'text'  => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_DATETIME ,
            'active' => Schema::TYPE_BOOLEAN,
            
        ]);

    }

    public function down()
    {


        $this->dropTable('news');
       

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
