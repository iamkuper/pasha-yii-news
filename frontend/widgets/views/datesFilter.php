<?php

use yii\helpers\Html;
use yii\helpers\Url;


?>

<?php if($filter != null): ?>
    <hr>
    <?php foreach($filter as $year): ?>
        <div>
            <?= Html::a($year['title'],  ['news/index', 'year'=> $year['url'] ], [ 'class' => $year['active'] ]) ?>
            <?php if(count($year['months']) > 0): ?>
                <?php foreach($year['months'] as $month): ?>
                    <?= Html::a($month['title'],  ['news/index', 'year'=> $month['url']['year'] , 'month'=> $month['url']['month'] ], [ 'class' => $month['active'] ]) ?>
                <?php endforeach; ?>
            <?php endif; ?> 
        </div>
    <?php endforeach; ?>
    <hr>
 <?php endif; ?>