<?php

namespace frontend\widgets;


class DateFilter extends \yii\bootstrap\Widget {

	public $dates = [];

	public $year  = false;
	public $month = false;

	public $filter = [];

	public function init(){

		parent::init();
		
	    foreach ($this->dates as $i => $item) {
	        if (!isset($this->filter[$item['year']])) {
	            $this->filter[$item['year']] = [
	                'url'    => ($this->year == (int)$item['year']) ? null : $item['year'] ,
	                'title'  => $item['year'],
	                'months' => [],
	                'active' => ($this->year == (int)$item['year']) ?  'active' : '',
	            ];
	          
	        } 
	        if ($this->year == (int)$item['year']) {
	        	$this->filter[$item['year']]['months'][$item['month']] = [
	                'url'    => ($this->month == (int)$item['month']) ? [ 'year' => $item['year'], 'month' => null ] : [ 'year' => $item['year'], 'month' => $item['month'] ],
	                'title'  =>  \Yii::$app->formatter->asDatetime($item['created_at'], 'MMMM'),
	                'active' => ($this->month == (int)$item['month']) ? 'active' : '',
	            ];
	        }   
        }





	}


	
	public function run(){

		return $this->render('datesFilter', [
			'filter'       => $this->filter,
		]);





	}
}
?>