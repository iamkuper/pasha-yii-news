<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\DateFilter;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\News */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">
    <h1><?= Html::encode($this->title) ?></h1>
   
    <?= DateFilter::widget($filter) ?>
   
    <?php if($dataProvider != null): ?>
        <?php foreach($dataProvider->models as $model): ?>
        <div>
            <h2><?= Html::a(Html::encode($model->title), 'news/'.$model->id ) ?></h2>
            <?= Html::encode($model->preview) ?><br>
            <p style="color: gray;"><?= Html::encode($model->created_at) ?></p>
            <hr>

        </div>
        <?php endforeach; ?>
    <?php endif; ?>

  

</div>
