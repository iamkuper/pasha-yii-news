<?php

namespace frontend\controllers;

use Yii;
use common\models\News;
use common\models\search\News as NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\ErrorException;
use yii\db\Query;

class NewsController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionView($id)
    {
    	return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function getFilter($year = false, $month = false) {

        $data = (new Query())
                    ->select(['created_at, YEAR(created_at) as year', 'MONTH(created_at) as month'])
                    ->from('news')
                    ->where(['active' => 1])
                    ->all();

        $out = ['dates' => $data ];

        if ($year)  $out['year'] = $year;
        if ($month)  $out['month'] = $month;

        return $out;
    }

    public function actionIndex($year = false, $month = false)
    {

        $searchModel = new NewsSearch();
        $searchModel->active = 1;

        return $this->render('index', [
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
            'filter'       => $this->getFilter($year, $month),
        ]);

    }

      /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
