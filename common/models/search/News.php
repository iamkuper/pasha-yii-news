<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\News as NewsModel;

/**
 * News represents the model behind the search form about `common\models\News`.
 */
class News extends NewsModel
{	

	public $active = false;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'active'], 'integer'],
            [['title', 'preview', 'text', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsModel::find();
        if ($this->active) $query->andFilterWhere([ 'active' => $this->active ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
        if (isset($params['year']))  $query->andFilterWhere(['YEAR(created_at)'  => $params['year']]);
        if (isset($params['month'])) $query->andFilterWhere(['MONTH(created_at)' => $params['month']]);


        if ( empty($params) || !$this->load($params) || !$this->validate() )
            return $dataProvider;

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'preview', $this->preview])
            ->andFilterWhere(['like', 'text', $this->text]);



        return $dataProvider;
    }
}
