<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $preview
 * @property string $text
 * @property string $created_at
 * @property integer $active
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['preview', 'text'], 'string'],
            [['created_at'], 'safe'],
            [['active'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $date = new \DateTime();
                $this->created_at = $date->format('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'preview' => 'Preview',
            'text' => 'Text',
            'created_at' => 'Created At',
            'active' => 'Active',
        ];
    }
}